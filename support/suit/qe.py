from tkinter import N


class Analysis:
    FQ_STATE_UNSTEADY = 11
    FQ_STATE_STABLE = 10
    FQ_STATE_EXTREMELY_UNSTABLE = 1
    FQ_STATE_NOT_SURE = 0

    @staticmethod
    def fourquadrant(pkloc, dkloc, qtp, qtd):
        effort = pkloc - qtp
        density = dkloc - qtd
        status = Analysis.FQ_STATE_NOT_SURE
        if(effort >= 0 and density >= 0):
            status = Analysis.FQ_STATE_UNSTEADY
        elif(effort >= 0 and density < 0):
            status = Analysis.FQ_STATE_STABLE
        elif(effort < 0 and density >= 0):
            status = Analysis.FQ_STATE_EXTREMELY_UNSTABLE
        return {"effort":effort, "density":density, "status":status}

class DI:
    def __init__(self, *, critical=10, major=3, minor=1, warning=0.5):
        self.weight = {"critical":critical, "major":major, "minor":minor, "warning":warning}
        self._critical = 0
        self._major = 0
        self._minor = 0
        self._warning = 0
    
    @property
    def critical(self, n=None):
        if(n is not None):
            self._critical = n
        return self._critical
    
    @property
    def major(self, n=None):
        if(n is not None):
            self._major = n
        return self._major

    @property
    def minor(self, n=None):
        if(n is not None):
            self._minor = n
        return self._minor
    
    @property
    def warning(self, n=None):
        if(n is not None):
            self._warning = n
        return self._warning

    def append(self, *, critical=0, major=0, minor=0, warning=0):
        self._critical += critical
        self._major += major
        self._minor += minor
        self._warning += warning
    
    @property
    def value(self):
        return self.critical * self.weight["critical"] + self.major * self.weight["major"] + self.minor * self.weight["minor"] + self.warning * self.weight["warning"]
    
    def density(self, effect):
        return round(self.get()/effect)


