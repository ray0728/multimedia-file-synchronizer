import subprocess
try:
    from rich.progress import track
    import openpyxl
except Exception as e:
    module = None
    print(str(e))
    if(e.name == 'rich'):
        module = 'Rich'
    elif(e.name == 'PIL'):
        module = 'Pillow'
    elif(e.name == 'openpyxl'):
        module = 'openpyxl'
    if(module):
        process = subprocess.Popen(['python3', '-m','pip','install',module])
        process.wait()
