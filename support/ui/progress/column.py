from rich.progress import BarColumn, DownloadColumn, Progress, TextColumn, TimeRemainingColumn, ProgressColumn
from typing import Optional
from rich.text import Text, TextType
from rich.table import Column

class CountColumn(ProgressColumn):

    def __init__(
        self, binary_units: bool = False, table_column: Optional[Column] = None
    ) -> None:
        self.binary_units = binary_units
        super().__init__(table_column=table_column)

    def render(self, task: "Task") -> Text:
        if(task.total is None):
            count_status = f"{task.completed}/?"
        else:
            total = int(task.total)
            count_status = f"{task.completed}/{total}"
        count_text = Text(count_status, style="progress.download")
        return count_text