import time, traceback, sys, os
from rich.console import Console
class Log:
    DEBUG = 0
    WARN = 1
    INFO = 2
    ERROR = 3
    CRITICAL = 4

    MODE_RICH = 0
    MODE_SYSTEM = 1

    TAG_FILTER = None

    Level = INFO
    Mode = MODE_RICH
    logRedirect = None
    console = Console()
    

    @staticmethod
    def __transTupleToStr(data):
        return ' '.join(map(lambda x:str(x),data))

    @staticmethod
    def e(tag, *objects):
        Log.__print(Log.ERROR, tag, objects)
    
    @staticmethod
    def d(tag, *objects):
        Log.__print(Log.INFO, tag, objects)

    @staticmethod
    def w(tag, *objects):
        Log.__print(Log.INFO, tag, objects)

    @staticmethod
    def i(tag, *objects):
        Log.__print(Log.INFO, tag, objects)

    @staticmethod
    def __print(level, tag, objects):
        if(Log.TAG_FILTER is not None and tag != Log.TAG_FILTER):
            return
        if(Log.Level <= level):
            label = "DEBUG"
            style = "bright_blue"
            if(level == Log.WARN):
                label = "WARN"
                style = "bright_yellow"
            elif(level == Log.INFO):
                label = "INFO"
                style = "default on default"
            elif(level == Log.ERROR):
                label = "ERROR"
                style = "bold bright_red"
            elif(level == Log.CRITICAL):
                label = "CRITICAL"
                style = "bold bright_red"
            if(Log.logRedirect):
                Log.logRedirect.recode(label, os.path.basename(tag), Log.__transTupleToStr(objects))
            elif(Log.Mode == Log.MODE_RICH):
                Log.console.log("[%s ]".format(label), os.path.basename(tag), Log.__transTupleToStr(objects), style="default on default")    
            else:
                print("[%s ]".format(label), os.path.basename(tag), Log.__transTupleToStr(objects))
            if(level >= Log.ERROR):
                exc_type, exc_value, exc_traceback = sys.exc_info()
                for info in traceback.format_tb(exc_traceback):
                    if(Log.logRedirect):
                        Log.logRedirect.recode("", "", info)
                    else:
                        Log.console.log(info)


class NullDev:
    def recode(self, *args):
        pass
